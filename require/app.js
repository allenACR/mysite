define("app", [	"custom", "angular", "router-ui", "ngSanitize", "ngAnimate", "uiUtil", 
				"ngCentered", "ngSticky", "ngEditable", "foundation5", "psResponsive", 
				"ngCarousel", "FBAngular", "loadingBar", "adaptive", "ngStorage", "imgFallback",
				"screenblur", "ngSmoothScroll", "ngFitText",
				"toaster", "firebase"], function(custom) { 
	custom.logger("Function : app");
	
	
	////////////////
	// INIT 
	var app = angular.module('app', [	"ui.router", "ngAnimate", "ngSanitize", "angular-centered", "sticky", "contenteditable", "mm.foundation", 
										"ui.utils", "psResponsive", "angular-carousel", "FBAngular", "chieffancypants.loadingBar", "dcbImgFallback",
										"stBlurredDialog", "SmoothScroll", "ngFitText",
										"adaptive.detection", "ngStorage", "toaster", "firebase"]);
	app.init = function () {
	      angular.bootstrap(document, ['app']);      
	};
	////////////////
	
	// let's make a nav called `myOffCanvas`
	app.factory('myOffCanvas', function (cnOffCanvas) {
	  return cnOffCanvas({
	    controller: 'headerCtrl',
	    templateUrl: 'components/offcanvas/offcanvas.html'
	  });
	}).
	
	// typically you'll inject the offCanvas service into its own
	// controller so that the nav can toggle itself
	controller('MyOffCanvasCtrl', function (myOffCanvas) {
	  this.toggle = myOffCanvas.toggle;
	}).
		
		
	directive('styleParent', function(){ 
	   return {
	     restrict: 'A',
	     link: function(scope, elem, attr) {
	         elem.on('load', function() {
	            var w = $(this).width(),
	                h = $(this).height();
				
					//$('#mainSlider').css('min-height', h + 'px');
					//$('#headerFiller').css('height', h + 'px'  ) ;
	            //check width and height and apply styling to parent here.
	         });
	     }
	   };
	}).	
		
	////////////////  CONFIG
	//
	config(function($stateProvider, $urlRouterProvider) {
			
			// For any unmatched url, redirect to /state1
			$urlRouterProvider.otherwise("/home");
		  
			// Now set up the states
			$stateProvider
			    .state('home', {
			      url: "/home",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      	
			        "slider": { templateUrl: "layout/slider/slider.html",
			        			controller: "sliderController"
			        },				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },			      	
			        "header": { templateUrl: "components/header/header.html",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "components/footer/footer.html",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/home/home.html",
			        			controller: "homeController"
			         },			         
			      },	
				  onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },				  			      		      
			      
			    })
			    
			    .state('about', {
			      url: "/about",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      			      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },				      	
			        "header": { templateUrl: "components/header/header.html",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "components/footer/footer.html",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/about/about.html",
			        			controller: "aboutController"
			         },
			      },	
				  onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },			      
			    })
			    
			    .state('portfolio', {
			      url: "/portfolio",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },				      	
			        "header": { templateUrl: "components/header/header.html",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "components/footer/footer.html",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/portfolio/portfolio.html",
			        			controller: "portfolioController"
			         },
			      },
			      onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },
			    })
			    
			    .state('hire', {
			      url: "/hire",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },				      	
			        "header": { templateUrl: "components/header/header.html",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "components/footer/footer.html",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/hire/hire.html",
			        			controller: "hireController"
			         },
			      },
				  onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },			      	
			    })
			    
			    .state('blog', {
			      url: "/blog",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },				      	
			        "header": { templateUrl: "components/header/header.html",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "components/footer/footer.html",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/carousel/carousel.html",
			        			controller: "carouselController"
			         },
			      },
				  onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },			      	
			    }); 			    
			    

	    			       
		});
		//
		////////////////   
	
	return app;
	
});
