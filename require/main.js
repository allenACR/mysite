// ESTABLISH REQUIREJS PATHING
require.config({
	
	// URL
	baseUrl: '',
	
	// LOCATION OF SCRIPTS
	paths: {
		// REQUIRED
		"jquery": "http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min",
		"foundation": "vendor/foundation5/js/foundation.min",				
		"foundation5": "vendor/foundation5/js/foundation5",
		"firebaseJS": "https://cdn.firebase.com/v0/firebase",
		"app": "require/app",
			
		// MODULES //	
		"angular": "vendor/angular/js/angular-1.2.2",
		"router-ui": "vendor/angular/module/angular-ui-router",
		"ngAnimate": "vendor/angular/module/angular-animation-1.2.2",
		"ngSanitize": "vendor/angular/module/angular-sanitize",
		"ngCentered": "vendor/angular/module/angular-centered",
		"ngSticky": "vendor/angular/module/sticky",
		"ngEditable": "vendor/angular/module/angular-contenteditable",
		"psResponsive": "vendor/angular/module/ps-responsive",
		"ngCarousel": "vendor/angular/module/angular-carousel.min",
		"ngTouch": "https://ajax.googleapis.com/ajax/libs/angularjs/1.2.9/angular-touch.min",
		"FBAngular": "vendor/angular/module/angular-fullscreen",
		"loadingBar": "vendor/angular/module/loading-bar",
		"ngStorage": "vendor/angular/module/ngStorage.min",
		"toaster": "vendor/angular/module/toaster",		
		"uiUtil": "vendor/angular/module/ui-utils.min",
		"adaptive": "vendor/angular/module/angular-adaptive-detection.min",
		"imgFallback": "vendor/angular/module/angular.dcb-img-fallback.min",
		"screenblur": "vendor/screenblur/js/st-blurred-dialog",
		"ngSmoothScroll": "vendor/angular/module/ng-smoothscroll.min",
		"ngFitText": "vendor/angular/module/ng-FitText",										//http://ngmodules.org/modules/ng-FitText.js
		
		// SERVICES //
		"custom": "vendor/angular/services/custom",
		"konami": "vendor/angular/services/konami",
		
		// FIREBASE //		
		"firebase": "https://cdn.firebase.com/libs/angularfire/0.7.1/angularfire.min",
		
		// LAYOUT //
		"overlayCtrl": "layout/overlay/overlay",
		"offcanvasCtrl": "layout/offcanvas/offcanvas",
		"sliderCtrl": "layout/slider/slider",
		
		// COMPONENTS		
		"headerCtrl": "components/header/header",
		"footerCtrl": "components/footer/footer",		
		"homeCtrl": "components/home/home",
		"aboutCtrl": "components/about/about",
		"portfolioCtrl": "components/portfolio/portfolio",
		"hireCtrl": "components/hire/hire",
		"underCtrl": "components/under/under",
	
	},

    //DEPENDENCIES
    shim: {        
        
        "angular": {
            exports: "angular"
        },
        "router-ui": {
        	deps: ['angular'],
            exports: "angular"
        },
        "ngSanitize": {
        	deps: ['angular'],
            exports: "angular"
        },          
        "uiUtil": {
        	deps: ['angular'],
            exports: "angular"
        },          
        "ngAnimate": {
        	deps: ['angular'],
        	exports: "angular"
        }, 
        "ngCentered": {
        	deps: ['angular'],
        	exports: "angular"
        },  
        "ngSticky": {
        	deps: ['angular'],
        	exports: "angular"
        },
        "ngEditable": {
        	deps: ['angular'],
        	exports: "angular"
        },  
        "psResponsive": {
        	deps: ['angular'],
        	exports: "angular"
        },    
        "ngTouch": {
        	deps: ['angular'],
        	exports: "angular"
        },    
        "ngCarousel": {
        	deps: ['angular', 'ngTouch'],
        	exports: "angular"	
        }, 
        "FBAngular": {
        	deps: ['angular', 'ngTouch'],
        	exports: "angular"	
        }, 
        "loadingBar": {
        	deps: ['angular', 'ngTouch'],
        	exports: "angular"	
        },  
        "ngStorage": {
        	deps: ['angular'],
        	exports: "angular"	
        },     
        "toaster": {
        	deps: ['angular'],
        	exports: "angular"	
        },     
        "adaptive": {
        	deps: ['angular'],
        	exports: "angular"
        }, 
        "imgFallback":{
        	deps: ['angular'],
        	exports: "angular"
        },
      	"screenblur":{
			deps: ['angular'],
        	exports: "angular"      		
      	},
    	"ngSmoothScroll":{
			deps: ['angular'],
        	exports: "angular"  	
    	},
    	"ngFitText":{
			deps: ['angular'],
        	exports: "angular"      		
    	},
                                               
        "foundation5": {
        	deps: ['angular'],
        	exports: "angular"
        },
        
        "firebase": {
			deps: ['angular', 'firebaseJS'],
        	exports: "angular"        	
        }     
    }	
	
});
 	 




// INITALIZE ANGULAR
require(	[	'app', 'firebase', 
				'overlayCtrl', 'offcanvasCtrl', 'sliderCtrl', 
				'headerCtrl', 'footerCtrl', 
				'homeCtrl','aboutCtrl', 'portfolioCtrl', 'hireCtrl', 'hireCtrl', 'underCtrl'
			], 
				
	function (	app, firebase, overlayCtrl, offcanvasCtrl, headerCtrl, footerCtrl, 
				homeCtrl, aboutCtrl, hireCtrl, underCtrl, portfolioCtrl
			 ){
	
	// START CONTROLLERS
	overlayCtrl.apply(app);
	offcanvasCtrl.apply(app);
	footerCtrl.apply(app);
	headerCtrl.apply(app);
	homeCtrl.apply(app);
	aboutCtrl.apply(app);
	hireCtrl.apply(app);
	underCtrl.apply(app);
	portfolioCtrl.apply(app);
	
	
	app.init();
	
});


