define(['custom'], function(custom, app) {
	var fileName  = 'footer';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope, $modal, cfpLoadingBar, stBlurredDialog, SmoothScroll, toaster) {	   
				    
					/* MODAL */			
					$scope.open = function () {	
					stBlurredDialog.open();
					var modalInstance = $modal.open({
					      	templateUrl: 'myModalContent.html',
					  		controller: ModalInstanceCtrl
					});
					
					modalInstance.result.then(function (selectedItem){}, 
						function () {
							stBlurredDialog.close();
					  		custom.logger('Modal dismissed at: ' + new Date());
					    });
					};
					/* END MODAL */	
					
					$scope.testScroll = function(){
					  SmoothScroll.$goTo(0).then(function() {
					    return SmoothScroll.$goTo('#trueBottom');
					  }).then(function() {
					    return SmoothScroll.$goTo(0);
					  }).then(function() {
					    return SmoothScroll.$goTo('#trueBottom');
					  }).then(function() {
					    return SmoothScroll.$goTo(0);
					  }).then(function() {
					    return SmoothScroll.$goTo('#trueBottom');
					  });
				   };
				    
				    $scope.fileName = fileName;
				  
				    $scope.master = {};
				
				    $scope.update = function(user) {
				      $scope.master = angular.copy(user);
				    };
				
				    $scope.reset = function() {
				      $scope.user = angular.copy($scope.master);
				    };
				
				    $scope.isUnchanged = function(user) {
				      return angular.equals(user, $scope.master);
				    };
				
				
					$scope.emailSent = false; 
					$scope.sendEmail = function(){
						var contact 		= $scope.user.name;
						var emailAddress 	= $scope.user.email;
						var themessage		= $scope.user.comment; 
						
						
						if (emailAddress != undefined){
							
							 /* RESET FORMS */
							 toaster.pop('success', "Email Sent", "Thanks for the email!  We'll respond as soon as possible!");
							 $scope.user.name = '';
							 $scope.user.email = '';
							 $scope.user.comment = '';
							 $scope.emailSent = true;
							 
					         $.ajax({ url: 'core/common/common.php',
					                 data: {action: "email", name: contact, email: emailAddress, message: themessage },
					                 type: 'post',
					                 success: function() {
									    															
					                 },
									 error: function(){
																	
									 }
					        });



						};				
					};
					


				    $scope.reset();
				   
				    
				});
				
				/* MODAL CONTROLLER */
				var ModalInstanceCtrl = function ($scope, $modalInstance, $detection, stBlurredDialog) {

					///////////////////////	 DETECTION   
				  	if ($detection.isAndroid()){
				  		$scope.detected = "media/detection/androidIcon.jpg";
				  		$scope.detectedType = "Android OS";
				  	}
				  	else if($detection.isiOS()){
				  		$scope.detected = "media/detection/iosIcon.jpg";
				  		$scope.detectedType = "IOS";
				  	}
				  	else if($detection.isWindowsPhone()){
				  		$scope.detected = "media/detection/windowsIcon.jpg";
				  		$scope.detectedType = "Windows 8 (Mobile)";
				  	}
				  	else{
				  		$scope.detected = "media/detection/desktopIcon.jpg";
				  		$scope.detectedType = "Desktop";
				  	}
					///////////////////////	   						
				
				  $scope.ok = function () {
				    $modalInstance.dismiss('cancel');
				  };
				
				  $scope.cancel = function () {
				    $modalInstance.dismiss('cancel');
				  };
				};	
				/* END MODAL CONTROLLER */	
				
				
				
							
								
	    },
	    ///////////////////////////////////////
  };
});
