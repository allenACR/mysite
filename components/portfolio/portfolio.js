define(['custom'], function(custom) {

	
	var fileName  = 'portfolio';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope, $timeout, cfpLoadingBar, $timeout) {	   
				    
				    $scope.headerMsg = "Portfolio"; 
				    $scope.aboutContent = "";

				    /*  NAVIGATION */
				    $scope.navIndex = 2;  //START ON SLIDE DEFAULT
		            $scope.navprev = function() {
		                $scope.navIndex--;
		                
		                if ( $scope.navIndex < 0){
		                	$scope.navIndex = $scope.pages.length - 1;
		                }
		            };
		            $scope.navnext = function() {
		                $scope.navIndex++;
		                if ($scope.navIndex > $scope.pages.length - 1){
		                	$scope.navIndex = 0;
		                }
		            };					    
				    $scope.pages = [
				    	{
							name: 'Home',
							route: 'home',
							icon: 'fa fa-home',
							description: "Return to the home page and feel that website magic all over again."		    	
					    },
					    {
							name: 'About',
							route: 'about',
							icon: 'fa fa-star',
							description: "We're a small team comprised mostly of dogs and awesome people."			    	
					    },
					    {
							name: 'Portfolio',
							route: 'portfolio',
							icon: 'fa fa-gears',	
							description: "This is just a small sample of what we're able to do.  Check it out!"		    	
					    },
					    {
							name: 'Hire Us',
							route: 'hire',
							icon: 'fa fa-money',
							description: "We can tackle projects large and small, personal or corporate.  Whatever you need we can make."				    	
					    }	
					    				    
				    ];
				    /* END NAVIGATION */


					//WEBSITES
				    $scope.websites = [
				    	{
					    	title: 	"Arclyte Imagery",					    	
					    	url: 	'www.arclyteimagery.com',
					    	image: 	'media/portfolio/wwwIcon.png',
					    	description: 'Photography at it\s finest.  That being said, if you ever need a wedding photographer (or just want some sexy pics), look no further.'
				    	},
				    	{
					    	title: 	"BrowsAR",				    	
					    	url: 	'www.browsar.com/',
					    	image: 	'media/portfolio/wwwIcon.png',
					    	description: 'Need to augment your reality with some Augmented Reality?  Of course you do.  Start an account with Gravityjack and check it out.'
				    	},
				    	{
					    	title: 	"First Rites Films",				    	
					    	url: 	'www.firstritesfilms.com',
					    	image: 	'media/portfolio/wwwIcon.png',
					    	description: 'You like independent movies/films/music videos?  Then this is for you.'
				    	},
				    	{
					    	title: 	"Gravityjack Estimator",					    	
					    	url: 	'www.freeappestimate.com/',
					    	image: 	'media/portfolio/wwwIcon.png',
					    	description: 'Wanna see how much it\'ll cost you to make that app of your dreams.  Check out this eye opening web app and cry.'
				    	}					    		
				    	
				    ];	
				    
				    // PROJECTS 
				    $scope.projects = [
				    	{
					    	title: 	"Simple Mockup",					    	
					    	url: 	'www.codeandlogic.com/sites/mock1/',
					    	image: 	'media/portfolio/wwwIcon.png',
					    	description: 'A fullscreen homepage mockup for basic or simple websites.'
				    	},	
				    	{
					    	title: 	"Corporate Mockup",					    	
					    	url: 	'www.codeandlogic.com/sites/mock2/',
					    	image: 	'media/portfolio/wwwIcon.png',
					    	description: 'A fullscreen homepage mockup for a corporate websites.'
				    	},	
				    	{
					    	title: 	"Artistic Mockup",					    	
					    	url: 	'www.codeandlogic.com/sites/mock3/',
					    	image: 	'media/portfolio/wwwIcon.png',
					    	description: 'An artsy design for artsy people.'
				    	},	
				    	{
					    	title: 	"Mobile Mockup",					    	
					    	url: 	'www.codeandlogic.com/sites/mock4/',
					    	image: 	'media/portfolio/wwwIcon.png',
					    	description: 'A design that works specifically well for mobile phones and other small screens.'
				    	},	
				    					    					    					    				    				    	{
					    	title: 	"Parallax Mockup",					    	
					    	url: 	'www.codeandlogic.com/sites/mock5/',
					    	image: 	'media/portfolio/wwwIcon.png',
					    	description: 'A modern design with built in parallax (for non-mobile browsers).  An excellent blend of sophistication and simplicity.'
				    	},	
				    	{
					    	title: 	"Bootstrap Kit",					    	
					    	url: 	'www.codeandlogic.com/sites/bootstrapKit/',
					    	image: 	'media/portfolio/wwwIcon.png',
					    	description: 'This kit comes with everything you need to make a Twitter Bootstrap inspired website quickly and easily.'
				    	},
				    	{
					    	title: 	"Foundation 5 Kit",				    	
					    	url: 	'www.codeandlogic.com/sites/foundation5Kit/',
					    	image: 	'media/portfolio/wwwIcon.png',
					    	description: 'Tired of the whole Twitter Bootstrap fad?  Me too.  That\'s why I made this sweet ZURB Foundation inspired kit.'
				    	},				    	
				    	{
					    	title: 	"AngularJS Kit",				    	
					    	url: 	'www.codeandlogic.com/sites/angularKit/',
					    	image: 	'media/portfolio/wwwIcon.png',
					    	description: 'Same as the other kits, but with amazing power of AngularJS components.  This thing has a power level OVER 9000!'
				    	},
				    	{
					    	title: 	"AngularJS Extended Kit",				    	
					    	url: 	'www.codeandlogic.com/sites/firebaseCMS/',
					    	image: 	'media/portfolio/wwwIcon.png',
					    	description: 'An even better, much more advanced version of the AngularJS Kit.  Includes several modules that make webdevelopment super fast and easy.'
				    	},				    	

				    	{
					    	title: 	"Falling Up",					    	
					    	url: 	'www.codeandlogic.com/sites/fallingUp/',
					    	image: 	'media/portfolio/wwwIcon.png',
					    	description: 'Just a quick and dirty example of parallax scrolling.  (Doesn\'t work on mobiles, unfortunately).'
				    	}					    		
				    	
				    ];	
				    
				    
				    // ENTRIES
				    $scope.entries = [
				    	{
					    	title: "Angular Icon",					    	
					    	image: 'media/home/responsive.png'
				    	},
				    	{
					    	title: "Clean Semantic Code",					    	
					    	image: 'media/home/code.png'
				    	},
				    				    					    	{
					    	title: "No Messy Backend (Giggidy)",					    	
					    	image: 'media/home/firebase.png'
				    	},
				    	{
					    	title: "Based in Tacoma Washington",					    	
					    	image: 'media/home/bear.png'
				    	}					    		
				    	
				    ];		
				    
				    
				    // ROW SLIDER
				    $scope.rowSlider = [
				    	{
					    	title: "Javascript | jQuery |HTML5 | CSS3",					    	
					    	image: 'media/portfolio/row1.png'
				    	},
				    	{
					    	title: "Bootstrap3 | AngularJS | Foundation 5",					    	
					    	image: 'media/portfolio/row2.png'
				    	},
				    	{
					    	title: "LESS | PHP | Firebase",					    	
					    	image: 'media/portfolio/row3.png'
				    	}				    		
				    	
				    ];						    		    
				    
				    $scope.slideIndex = 0; 
		            // demo with controls
		            $scope.prev = function() {
		                $scope.slideIndex--;
		                
		                if ( $scope.slideIndex < 0){
		                	$scope.slideIndex = $scope.entries.length - 1;
		                }
		            };
		            $scope.next = function() {
		                $scope.slideIndex++;
		                if ($scope.slideIndex > $scope.entries.length - 1){
		                	$scope.slideIndex = 0;
		                }
		            };				    
				    
					// INIT
					$scope.init = function(){
						 cfpLoadingBar.start();
						 custom.parallaxStart();
						 $('body').scrollTop(0);  
					};

				    // fake the initial load so first time users can see it right away:
				    $timeout(function() {
				      cfpLoadingBar.complete();
				    }, 750);
				   
				    
				});				
	    },
	    ///////////////////////////////////////
  };
});
