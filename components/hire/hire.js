define(['custom'], function(custom) {

	
	var fileName  = 'hire';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope) {	   
				    
				    
				$scope.init = function(){
					 custom.parallaxStart();	
					 $('body').scrollTop(0);
				};				    
				    
				    $scope.headerMsg = "Hire Me"; 
				    $scope.aboutContent = "";

				    /*  NAVIGATION */
				    $scope.navIndex = 3;  //START ON SLIDE DEFAULT
		            $scope.navprev = function() {
		                $scope.navIndex--;
		                
		                if ( $scope.navIndex < 0){
		                	$scope.navIndex = $scope.pages.length - 1;
		                }
		            };
		            $scope.navnext = function() {
		                $scope.navIndex++;
		                if ($scope.navIndex > $scope.pages.length - 1){
		                	$scope.navIndex = 0;
		                }
		            };					    
				    $scope.pages = [
				    	{
							name: 'Home',
							route: 'home',
							icon: 'fa fa-home',
							description: "Return to the home page and feel that website magic all over again."		    	
					    },
					    {
							name: 'About',
							route: 'about',
							icon: 'fa fa-star',
							description: "We're a small team comprised mostly of dogs and awesome people."			    	
					    },
					    {
							name: 'Portfolio',
							route: 'portfolio',
							icon: 'fa fa-gears',	
							description: "This is just a small sample of what we're able to do.  Check it out!"		    	
					    },
					    {
							name: 'Hire Us',
							route: 'hire',
							icon: 'fa fa-money',
							description: "We can tackle projects large and small, personal or corporate.  Whatever you need we can make."				    	
					    }	
					    				    
				    ];
				    /* END NAVIGATION */
				    
				});		
				
				


				
				
										
	    },
	    ///////////////////////////////////////
  };
});
