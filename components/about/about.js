define(['custom'], function(custom) {

	
	var fileName  = 'about';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope, $timeout, cfpLoadingBar, $timeout) {	   
				    
				    $scope.headerMsg = "About Us"; 
				    $scope.aboutContent = "My name is Allen Royston and I love programming.  Yeah, it SOUNDS boring, but it's my thing.";
				    
				    /*  NAVIGATION */
				    $scope.navIndex = 1;  //START ON SLIDE DEFAULT
		            $scope.navprev = function() {
		                $scope.navIndex--;
		                
		                if ( $scope.navIndex < 0){
		                	$scope.navIndex = $scope.pages.length - 1;
		                }
		            };
		            $scope.navnext = function() {
		                $scope.navIndex++;
		                if ($scope.navIndex > $scope.pages.length - 1){
		                	$scope.navIndex = 0;
		                }
		            };					    
				    $scope.pages = [
				    	{
							name: 'Home',
							route: 'home',
							icon: 'fa fa-home',
							description: "Return to the home page and feel that website magic all over again."		    	
					    },
					    {
							name: 'About',
							route: 'about',
							icon: 'fa fa-star',
							description: "We're a small team comprised mostly of dogs and awesome people."			    	
					    },
					    {
							name: 'Portfolio',
							route: 'portfolio',
							icon: 'fa fa-gears',	
							description: "This is just a small sample of what we're able to do.  Check it out!"		    	
					    },
					    {
							name: 'Hire Us',
							route: 'hire',
							icon: 'fa fa-money',
							description: "We can tackle projects large and small, personal or corporate.  Whatever you need we can make."				    	
					    }	
					    				    
				    ];
				    /* END NAVIGATION */
				    
					// ABOUT DATA
				    $scope.aboutUs = [
				    	{
				    		title: "CEO",
					    	name: "Tuxedo Wellington the Third",					    	
					    	image: 'media/about/tux.jpg',
					    	description: 'Tuxedo is the proud owner of Code And Logic who founded the company in 2012 after deciding that sniffing butt and chasing rabbits just wasn\'t his thing anymore.  He is an avid fan of the arts and is often seen wagging his tail when attending the local theather.'
				    	},
				    	{
				    		title: "CTO",
					    	name: "Bowtie",					    	
					    	image: 'media/about/bowtie.jpg',
					    	description: 'New to the company is Bowtie.  Being our Chief Sales Officer, Bowtie has managed to look cute, do the dog head-tilt thing, but has failed to capture the heart of our landlord, whom - mind you - really doesn\'t care much for us.'
				    	},
				    	{
				    		title: "Front-End Developer",
					    	name: "Allen Royston",					    	
					    	image: 'media/about/allen.jpg',
					    	description: 'A mix of freelance and custom software experience rounds out our sexy, handsome, all too perfect front-end developer who in no way wrote his own entry.  He has a *perfectly rational* fear of bears even though the only ones he has seen are on Youtube (and are adorable).'
				    	},
				    	{
				    		title: "Designer / Photographer / Girlfriend",
					    	name: "Angela Clark",					    	
					    	image: 'media/about/angie.jpg',
					    	description: 'An avid photographer and skilled designer, Angie is a woman of many amazing talents.  She has an unnatural aversion to fruits & vegitables and will not eat them unless you sneak it into some chocolate first.'
				    	}					    		
				    	
				    ];	
				    
				    // ROW SLIDER
				    $scope.imageGallery = [
				    	{
					    	title: "I have a blackbelt in longboard-jitsu.",					    	
					    	image: 'media/about/gallery1.jpg'
				    	},
				    	{
					    	title: "Pretty sure I had grass in my mouth.",					    	
					    	image: 'media/about/gallery2.jpg'
				    	},
				    	{
					    	title: "Does not approve.",					    	
					    	image: 'media/about/gallery3.jpg'
				    	},
				    	{
					    	title: "Snuggling my boss.",					    	
					    	image: 'media/about/gallery4.jpg'
				    	},
				    	{
					    	title: "Something about my hair makes this feel like a 90s family sitcom...",					    	
					    	image: 'media/about/gallery5.jpg'
				    	},
				    	{
					    	title: "Cheeese!",					    	
					    	image: 'media/about/gallery6.jpg'
				    	}							    						    					    
				    ];	
				  
				    			    
				    
				    $scope.slideIndex = 0; 
		            // demo with controls
		            $scope.prev = function() {
		                $scope.slideIndex--;
		                
		                if ( $scope.slideIndex < 0){
		                	$scope.slideIndex = $scope.imageGallery.length - 1;
		                }
		            };
		            $scope.next = function() {
		                $scope.slideIndex++;
		                if ($scope.slideIndex > $scope.imageGallery.length - 1){
		                	$scope.slideIndex = 0;
		                }
		            };				    
				    
					// INIT
					$scope.init = function(){
						 cfpLoadingBar.start();
						 custom.parallaxStart();
						 $('body').scrollTop(0);	  
					};

				    // fake the initial load so first time users can see it right away:
				    $timeout(function() {
				      cfpLoadingBar.complete();
				    }, 750);
				   
				    
				});				
	    },
	    ///////////////////////////////////////
  };
});
