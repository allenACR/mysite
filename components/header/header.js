

define(['custom', 'konami'], function(custom, konami, app) {
	var fileName  = 'header';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
			
			
			
				app.controller(fileName + 'Controller', function($scope, $rootScope, SmoothScroll, psResponsive, $localStorage, $sessionStorage, $detection, stBlurredDialog) {	   
				   $scope.fileName = fileName;
				  
					$scope.$storage = $localStorage.$default({
					    name: "Guest"
					});		
					
					
					$scope.scrollHere = function(id){
						var pos = $('#' + id).position().top;
						var scrollValue = pos; 
						if ($('body').scrollTop() == 0){
							scrollValue = pos - 75;	
						}else{
							scrollValue = pos - 40;	
						}
						
						
						
						SmoothScroll.$goTo(scrollValue);
					};
				  
				 
				  
				  	// DETECT AND APPLY STICKY IF IT WORKS
				  	if ($detection.isAndroid()){
				  		$scope.stickyWorks = true;
				  	}
				  	else if($detection.isiOS()){				  		
				  		$scope.stickyWorks = false;
				  	}
				  	else if($detection.isWindowsPhone()){
				  		$scope.stickyWorks = true;
				  	}
				  	else{				  		
				  		// PC OR DESKTOP
				  		$scope.stickyWorks = true;
				  	}
				   
				    $rootScope.responsive = psResponsive;
					$scope.offcanvasToggle = function(){
						 stBlurredDialog.open();	
							custom.offcanvas('toggle');
					};
				   

				});				
	    },
	    ///////////////////////////////////////
  };
});
